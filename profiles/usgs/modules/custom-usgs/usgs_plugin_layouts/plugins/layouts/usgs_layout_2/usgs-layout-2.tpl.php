<?php
  //dsm(get_defined_vars());
?>

<div class="panel-display panel-usgs-layout-2 clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="row">
	  <div class="col-md-12 panel-panel panel-region-carousel">
	  	<div class="inside"><?php print $content['carousel']; ?></div>
	  </div>
	</div>
	<div class="row">
		<div class="col-md-2 panel-panel panel-region-left">
	    <div class="inside"><?php print $content['left']; ?></div>
	  </div>
	  <div class="col-md-10 panel-panel panel-region-main">
      <div class="inside"><?php print $content['main']; ?></div>
    </div>
	</div>
</div>
