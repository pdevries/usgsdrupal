<?php

/**
 * @file
 * template.php
 */

/**
 * Include preprocesser functions for overriden views theme templates.
 */
include_once dirname(__FILE__) . '/templates/views/theme.inc';

/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function palladium_bootstrap_preprocess_html(&$variables) {
    // Doesn't do anything now since the classes are hardcoded in
    // html.tpl.php
}

/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function palladium_bootstrap_preprocess_page(&$variables) {
  // We don't use the menus since we are going to use the taxonomy. Turn off all other
  // menus so they do not appear.
  unset($variables['primary_nav']);
  unset($variables['page']['navigation']);

  // Add our theme specific classes
  $variables['header_classes_array'] = array();

  $variables['navbar_classes_array'] = array('row');
  $variables['navbar_classes_array'][] = 'navbar-usgs';
  $variables['navbar_classes_array'][] = 'yamm';

  if(!isset($variables['navbar'])) {
  	$variables['navbar'] = '';
  }
  if(!isset($variables['usgs_admin_messages'])) {
  	$variables['usgs_admin_messages'] = '';
  }

  // Pad the top if the admin_menu is enabled, this just prevents things from overlapping
 	$variables['pad_top'] = '';
  if(module_exists('admin_menu') && user_is_logged_in()) {
  	$variables['pad_top'] = ' pad-top';
  }
}

/**
 * Implements hook_theme().
 *
 * Register theme hook implementations.
 *
 * The implementations declared by this hook have two purposes: either they
 * specify how a particular render array is to be rendered as HTML (this is
 * usually the case if the theme function is assigned to the render array's
 * #theme property), or they return the HTML that should be returned by an
 * invocation of theme().
 *
 * This was taken directly from the boostrap base theme's template.php file
 *
 * @see template.php of the bootstrap base theme
 * @see _bootstrap_theme()
 */
function palladium_bootstrap_theme(&$existing, $type, $theme, $path) {
  $hook_theme = array(
    'palladium_bootstrap_sub_menu' => array(
      'function' => 'palladium_bootstrap_sub_menu',
      'render element' => 'tree',
    ),
  );

  bootstrap_hook_theme_complete($hook_theme, $theme, $path . '/theme');

  return $hook_theme;
}

/**
 * Implements hook_theme_registry_alter()
 */
function palladium_bootstrap_theme_registry_alter(&$existing) {
    $include = drupal_get_path('theme', 'palladium_bootstrap') . '/theme/menu/menu-tree.func.php';
    $existing['menu_tree__primary']['function'] = 'palladium_bootstrap_menu_tree__primary';
    $existing['menu_tree__primary']['includes'][] = $include;
    $existing['menu_tree__secondary']['function'] = 'palladium_bootstrap_menu_tree__primary';
    $existing['menu_tree__secondary']['includes'][] = $include;
}

/**
 * Helper function that sets the #theme_wrapper for any items in a menu that
 * has children.
 *
 * @param string $elements
 * @return array
 */
function _palladium_bootstrap_check_for_sub_menu(&$elements, $recursing = FALSE, &$parent = array()) {
    $info = array();

    $children = element_children($elements);
    $elements['#children'] = isset($elements['#children']) ? $elements['#children'] : '';
    //dsm($children);
    $markup = '';
    foreach($children as $mlid) {
        if (count($elements[$mlid]['#below']) > 0) {
            unset($elements[$mlid]['#theme']);
            $elements[$mlid]['#theme_wrappers'][] = 'palladium_bootstrap_sub_menu';
            //dsm('Parent');
            //dsm($elements[$mlid]);
            _palladium_bootstrap_check_for_sub_menu($elements[$mlid]['#below'], TRUE, $elements[$mlid]);
        }
        else {
            $elements['#children'] .= theme('menu_link__main_menu', $elements[$mlid]);
        }
    }
    if(!empty($parent)) {
        if(!isset($parent['#children'])) {
            $parent['#children'] = '';
        }
        else {
            $parent['#children'] .= $elements['#children'];
        }
    }
    return $info;
}

/**
 * Implements hook_process_page().
 *
 * @see page.tpl.php
 */
function palladium_bootstrap_process_page(&$variables) {
  // The bootstrap theme declares the classes in an array in the preprocess function
  // then implodes them here for sending to the actual template. I follow the same
  // procedure to maintain consistency and I assume it is a good practice.
  $variables['header_classes'] = implode(' ', $variables['header_classes_array']);

  if(isset($variables['node'])) {
  	$node = $variables['node'];
  	if($node->type == 'hub_page') {
  		if(empty($node->og_group_ref)) {
  			//$variables['title'] = 'Overview';
  		}
  	}
  }
}

/**
 * Returns HTML for the image browser field in the usgs_object content type.
 *
 * @param array $variables
 * @return string
 */
function palladium_bootstrap_field__field_image_media_browser___usgs_object($variables) {
  $variables['classes'] .= ' media-object pull-left';
  $output = '';

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<div class="pull-left media-object">';
    $output .= drupal_render($item);
    $output .= '</div>';
  }

  return $output;
}

function palladium_bootstrap_field__body__usgs_object($variables) {
  // Render the items.
  $output = '<div class="media-body">';
  $output .= '<div class="media-heading"><h4 class="usgs-h4">' . $variables['element']['#object']->title . '</h4></div>';
  foreach ($variables['items'] as $delta => $item) {
    $output .= drupal_render($item);
  }
  $output .= '</div>';

  return $output;
}

/**
 * Returns HTML for the image browser field in the usgs_object content type.
 *
 * @param array $variables
 * @return string
 */
function palladium_bootstrap_field__field_image_media_browser___science($variables) {
  $variables['classes'] .= ' media-object pull-left';
  $output = '';

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<div class="pull-left media-object">';
    $output .= drupal_render($item);
    $output .= '</div>';
  }

  return $output;
}

function palladium_bootstrap_field__body__science($variables) {
  $item = menu_get_item();
  //dsm($item);

  $node = $variables['element']['#object'];

  if($item['map'][0] == 'usgs-admin' && $item['number_parts'] == 4) {
    $mission = $item['original_map'][1];
    $program = $item['original_map'][2];
    $section = $item['original_map'][3];

    $path = "usgs-admin/$mission/$program/$section/edit/{$node->nid}";
    $title = l($node->title, $path, array('query' => array('destination' => $item['path'])));
  }
  else {
    $title = $node->title;
  }
  // Render the items.
  $output = '<div class="media-body">';

  $output .= '<div class="media-heading"><h4 class="usgs-h4">' . $title . '</h4></div>';
  foreach ($variables['items'] as $item) {
    $output .= drupal_render($item);
  }
  $output .= '</div>';

  return $output;
}
